#include "lexer.h"
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>
#include <stdio.h>

typedef enum {
    EmptyContext,
    NumberContext,
    StringContext,
    WordContext,
    OperatorContext
} lexer_context ;

bool is_operator(char c) {
    static char *ops = "=<>|";
    for (char *i = ops; *i; i++)
        if (c == *i) return true;
    return false;
}

inline bool is_number(char *lexeme, size_t len) {
    for (size_t i = 0; i < len; i++)
        if (!isdigit(lexeme[i])) return false;
    return true;
}

token_kind get_longest_operator(char *chain, size_t len, size_t *match) {
    if (len >= 2 && strncmp(chain, "<<", 2) == 0) {
        *match = 2;
        return DAngleLeft;
    }
    if (len >= 2 && strncmp(chain, ">>", 2) == 0) {
        *match = 2;
        return DAngleRight;
    }
    if (strncmp(chain, "<", 1) == 0) {
        *match = 1;
        return AngleLeft;
    }
    if (strncmp(chain, ">", 1) == 0) {
        *match = 1;
        return AngleRight;
    }
    if (strncmp(chain, "=", 1) == 0) {
        *match = 1;
        return Equal;
    }
    if (strncmp(chain, "|", 1) == 0) {
        *match = 1;
        return Pipe;
    }
    else return None;
}

void split_operators(char *code, size_t start, size_t end, token_t *stream, size_t *stream_i, size_t line, size_t column) {
    size_t i = 0;
    size_t match = 0;
    while (i < (end - start)) {
        token_kind kind = get_longest_operator(code + start + i, (end - start) - i, &match);
        token_t token = { .kind = kind, .line = line, .column = column + i };
        stream[(*stream_i)++] = token;
        i += match;
    }
}

void make_token(lexer_context ctx, char *code, size_t *start, size_t *end, token_t *stream, size_t *stream_i, size_t line, size_t column) {
    if (*start >= *end) return;
    token_kind kind = None;
    if (ctx == OperatorContext) {
        split_operators(code, *start, *end, stream, stream_i, line, column);
        *start = *end;
        return;
    } else if (ctx == NumberContext) {
        kind = Number;
    } else if (ctx == WordContext) {
        kind = Word;
    } else if (ctx == StringContext) { 
        kind = String;
    }
    token_t token = { 
        .kind = kind, .line = line, .column = column, 
        .len = *end - *start };
    stream[*stream_i] = token;
    strncpy(stream[(*stream_i)++].lexeme, code + *start, *end - *start);
    *start = *end;
}

int32_t scan(char *code, token_t *stream, lexer_error_t *error) {
    size_t i = 0;
    size_t start = 0, end = 0;
    size_t stream_i = 0;
    size_t line = 1, column = 1;
    bool escape = false;
    lexer_context ctx;
    while (code[i] != '\0') {
        if (code[i] == '\\') {
            escape = !escape;
        }
        if (code[i] == ' ' || code[i] == '\t') {
            if (ctx != StringContext) {
                make_token(ctx, code, &start, &end, stream, &stream_i, line, column);
                ctx = EmptyContext;
                start = i + 1;
            }
        } else if (code[i] == '\n') {
            if (ctx != String) {
                make_token(ctx, code, &start, &end, stream, &stream_i, line, column);
                token_t newline = { .kind = NewLine, .line = line, .column = column };
                stream[stream_i++] = newline;
                column = 1;
                line ++;
                start = i + 1;
                ctx = EmptyContext;
            }
        } else if (is_operator(code[i])) {
            if (ctx != EmptyContext && ctx != OperatorContext && ctx != StringContext) {
                make_token(ctx, code, &start, &end, stream, &stream_i, line, column);
                ctx = OperatorContext;
            }
            if (ctx != StringContext)
                ctx = OperatorContext;
        } else if (isdigit(code[i])) {
            if (ctx != EmptyContext && ctx != NumberContext && ctx != WordContext && ctx != StringContext) {
                make_token(ctx, code, &start, &end, stream, &stream_i, line, column);
                if (ctx != WordContext) ctx = NumberContext;
            }
        } else if (code[i] == '\"') {
            if (escape) escape = false;
            else {
                make_token(ctx, code, &start, &end, stream, &stream_i, line, column);
                start = i + 1;
                if (ctx == StringContext) ctx = EmptyContext;
                else ctx = StringContext;
            }
        } else {
            if (ctx != StringContext)
                ctx = WordContext;
        }
        end++;
        i++;
        column ++;
    }
    if (start < end) {
        make_token(ctx, code, &start, &end, stream, &stream_i, line, column);
    }
    return stream_i;
}
