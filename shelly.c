#include <dirent.h>
#include <errno.h>
#include <limits.h>
#include <pwd.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include "lexer.h"
#include "list.h"
#include "parser.h"
#include "redir.h"

/**
 * @brief Gets the username and home directory of the current user
 * @param username The username buffer (must be atleast 32 bytes long)
 * @param homedir The home directory buffer (must be atleast PATH_MAX bytes
 *long)
 **/
void get_user(char *username, char *homedir) {
    struct passwd pwd;
    struct passwd *result;
    size_t buflen = sysconf(_SC_GETPW_R_SIZE_MAX);
    if (buflen == -1) buflen = 16384;
    char buf[buflen];
    int s = getpwuid_r(getuid(), &pwd, buf, buflen, &result);
    if (result == NULL) {
        if (s == 0)
            fprintf(stderr, "Failed to get user name!\n");
        else {
            errno = s;
            fprintf(stderr, "Call to getpwuid_r failed!\n");
        }
        exit(-1);
    }
    strcpy(username, pwd.pw_name);
    strcpy(homedir, pwd.pw_dir);
}

/**
 * @brief Expands the ~ character.
 * Both parameters are assumed to be of size PATH_MAX
 * @param path Pointer to the path to be expanded
 * @param expanded Pointer to the buffer where the expanded path is to be stored
 * @return pointer to expanded if expansion was performed,
 * or pointer to the provided path in case expansion was not found to be
 * necessary (no ~ in path)
 */
char *expand_home(char *path, char *expanded) {
    char user[128], home[PATH_MAX];
    int i = 0;
    while (path[i] != '\0') {
        if (path[i] == '~') {
            get_user(user, home);
            memset(expanded, 0, PATH_MAX);
            strncpy(expanded, path, i);
            strcat(expanded, home);
            strcat(expanded, path + i + 1);
            return expanded;
        }
        i++;
    }
    return path;
}

/**
 * @brief Prints greeting message
 **/
void greet() {
    const char *msg =
        " ####  #    # ###### #      #      #   # \n"
        "#      #    # #      #      #       # #  \n"
        " ####  ###### #####  #      #        #   \n"
        "     # #    # #      #      #        #   \n"
        "#    # #    # #      #      #        #   \n"
        " ####  #    # ###### ###### ######   #   \n";
    printf("\033c");
    printf("%s", msg);
    printf("Welcome to shelly version 0.1a!\n");
}

/**
 * @brief Prints usage rules of available shell features
 **/
void help() {
    printf(
        "Available commands:\n"
        "help: Prints this message\n"
        "cd <dir?>: Change directory to dir (defaults to home)\n"
        "ls <dir?>: List contents of dir (defaults to current working "
        "directory)\n"
        "touch <filename>: Create or refresh a file\n"
        "cat <filename>: Print contents of a file into standard output\n"
        "echo <message>: Print something to the console\n"
        "exit: Exit shell\n");
}

void cd(char *arg) {
    char user[128], home[PATH_MAX];
    get_user(user, home);
    if (arg == NULL) {
        int s = chdir(home);
        if (s == -1) printf("failed to change directory!\n");
    } else {
        char expanded[PATH_MAX];
        char *target = expand_home(arg, expanded);
        DIR *ent = opendir(target);
        if (ent == NULL) {
            if (errno == ENOENT) {
                fprintf(stderr, "%s: directory not found!\n", target);
            } else if (errno == ENOTDIR) {
                fprintf(stderr, "%s is not a directory!\n", target);
            }
        }
        closedir(ent);
        int s = chdir(target);
        if (s == -1) fprintf(stderr, "%d failed to change directory!\n", errno);
    }
}

void ls(char *arg) {
    char expanded[PATH_MAX] = "./";
    char *target = NULL;
    if (arg == NULL)
        target = expanded;
    else
        target = expand_home(arg, expanded);
    DIR *dir = opendir(target);
    if (dir == NULL) {
        if (errno == ENOENT) {
            fprintf(stderr, "%s: directory not found!\n", target);
        } else if (errno == ENOTDIR) {
            fprintf(stderr, "%s is not a directory!\n", target);
        } else {
            fprintf(stderr, "%d error reading directory %s!\n", errno, target);
        }
    } else {
        struct dirent *ent;
        while ((ent = readdir(dir))) {
            printf("%s\n", ent->d_name);
        }
        closedir(dir);
    }
}

void make_dir(char *arg) {
    char expanded[PATH_MAX];
    char *target = expand_home(arg, expanded);

    struct stat st = {0};

    if (stat(target, &st) == -1) {
        if (mkdir(target, 0700) == -1) {
            fprintf(stderr, "failed to create directory %s\n", target);
        }
    } else {
        fprintf(stderr, "directory already exists!\n");
    }
}

void touch(char *arg) {
    if (arg == NULL) {
        fprintf(stderr, "expected an argument!\n");
    }
    char expanded[PATH_MAX];
    char *target = expand_home(arg, expanded);
    FILE *file = fopen(target, "r");
    if (file) return;
    file = fopen(target, "w");
    if (file) return;
    fprintf(stderr, "failed to create file!\n");
}

void cat(char *arg) {
    char expanded[PATH_MAX];
    char *target = expand_home(arg, expanded);
    FILE *file = fopen(target, "r");
    if (file) {
        char buf[1024];
        while (fgets(buf, 1024, file)) {
            fputs(buf, stdout);
        }
    } else {
        fprintf(stderr, "failed to open file %s!\n", target);
    }
}

bool execute_simple_command(ast_node_t *root) {
    if (root == NULL) return false;
    char *cmd = ((token_t *)root->left->value)->lexeme;
    list_node_t *arglist = NULL;
    char **args = malloc(sizeof(char*) * 32);
    size_t arglen = 0;
    if (root->right && root->right->kind == ArgumentList) {
        arglist = (list_node_t *)root->right->value;
        if (arglist) {
            list_node_t *iter = arglist->next;
            args[arglen++] = cmd;
            while (iter) {
                args[arglen++] = (char*)((token_t*)iter->data)->lexeme;
                iter = iter->next;
            }
            args[arglen] = NULL;
        }
    }
    if (strcmp(cmd, "exit") == 0) {
        if (arglist)
            clear(arglist);
        if (args) free(args);
        return true;
    } else if (strcmp(cmd, "echo") == 0) {
        if (arglen > 1) {
            int i = 1;
            while (i < arglen) {
                printf("%s ", args[i++]);
            }
            printf("\n");
        } else {
            fprintf(stderr, "expected an argument!\n");
        }
    } else if (strcmp(cmd, "ls") == 0) {
        if (arglen > 1) {
            int i = 1;
            while (i < arglen) {
                printf("%s:\n", args[i]);
                ls(args[i++]);
            }
        } else {
            ls(NULL);
        }
    } else if (strcmp(cmd, "mkdir") == 0) {
        if (arglen > 1) {
            int i = 1;
            while (i < arglen) {
                make_dir(args[i++]);
            }
        } else {
            fprintf(stderr, "expected an argument!\n");
        }
    } else if (strcmp(cmd, "touch") == 0) {
        if (arglen > 1) {
            int i = 1;
            while (i < arglen) {
                touch(args[i++]);
            }
        } else {
            fprintf(stderr, "expected an argument!\n");
        }
    } else if (strcmp(cmd, "cat") == 0) {
        if (arglen > 1) {
            int i = 1;
            while (i < arglen) {
                cat(args[i++]);
            }
        } else {
            fprintf(stderr, "expected an argument!\n");
        }
    } else if (strcmp(cmd, "cd") == 0) {
        if (arglen > 1) cd(args[1]);
        else cd(NULL);
    } else if (strcmp(cmd, "pwd") == 0) {
        char buf[PATH_MAX];
        if (arglen > 1) fprintf(stderr, "too many arguments!\n");
        else {
            if (getcwd(buf, PATH_MAX) == NULL)
                printf("failed to get current working directory!\n");
            else
                printf("%s\n", buf);
        }
    } else if (strcmp(cmd, "help") == 0) {
        if (arglen > 1) fprintf(stderr, "too many arguments!\n");
        else help();
    } else if (strcmp(cmd, "clear") == 0) {
        if (arglen > 1) fprintf(stderr, "too many arguments!\n");
        else printf("\033c");
    } else {
        int pid = fork();
        int status;
        if (pid == 0) {
            if (execvp(cmd, args) == -1) {
                perror(cmd);
                exit(-1);
            }
        } else {
            waitpid(pid, &status, WUNTRACED);
            printf("child command status %d\n", status);
        }
    }
    if (args) free(args);
    if (arglist)
        clear(arglist);
    arglist = NULL;
    return false;
}

int redirect(int old, int new, bool keep) {
    int saved = -1;
    if (keep) saved = dup(old);
    dup2(new, old);
    close(new);
    return saved;
}

int bind_redirections(void *chain, int redfd[32]) {
    list_node_t *head = (list_node_t *)chain;
    if (head == NULL) {
        fprintf(stderr, "list cannot be empty!\n");
        exit(-1);
    }
    int i = 0;
    head = head->next;
    while (head) {
        ast_node_t *redir = (ast_node_t *)head->data;
        token_t *t = (token_t *)redir->value;
        token_t *filename_token = (token_t *)redir->right->value;
        char *filename = filename_token->lexeme;
        int old, new;
        if (t->kind == AngleRight) {
            old = STDOUT_FILENO;
            FILE *f = fopen(filename, "w");
            new = fileno(f);
        } else if (t->kind == DAngleRight) {
            old = STDOUT_FILENO;
            FILE *f = fopen(filename, "a");
            new = fileno(f);
        } else if (t->kind == AngleLeft) {
            old = STDIN_FILENO;
            FILE *f = fopen(filename, "r");
            new = fileno(f);
        } else if (t->kind == DAngleLeft) {
            old = STDIN_FILENO;
            FILE *f = fopen(filename, "r");
            new = fileno(f);
        }
        redfd[i++] = redirect(old, new, true);
        redfd[i++] = old;
        head = head->next;
    }
    return i;
}

void unbind_redirections(int fds[32], int len) {
    int i = 0;
    while (i < len) {
        int saved = fds[i++];
        int old = fds[i++];
        redirect(old, saved, false);
    }
}

bool execute_command(char *cmd) {
    if (cmd == NULL || strlen(cmd) == 0) return false;
    ast_node_t *ast = parse(cmd);
    if (ast) {
        switch (ast->kind) {
            case Command: {
                int fds[32];
                int fd_len = bind_redirections(ast->right->value, fds);
                execute_simple_command(ast->left);
                fflush(stdout);
                fflush(stdin);
                unbind_redirections(fds, fd_len);
                break;
            }
            case SimpleCommand:
                return execute_simple_command(ast);
                break;
            case Assignment:
                // TODO: implement
                break;
            default:
                fprintf(stderr, "Don't know how to execute command:\n%s", cmd);
                break;
        }
    }
    return false;
}

bool prompt() {
    // user@hostname [wd] $
    char username[128];
    char homedir[PATH_MAX];
    char hostname[128];
    char wd[PATH_MAX];
    char input_buf[512];
    get_user(username, homedir);
    if (gethostname(hostname, 128) == -1) {
        fprintf(stderr, "failed to get hostname!\n");
        return true;
    }
    if (getcwd(wd, PATH_MAX) == NULL) {
        fprintf(stderr, "failed to get working directory!\n");
        return true;
    }
    printf("\033[32m%s\033[0m@\033[32m%s \033[36m[%s]\033[0m $ ", username,
           hostname, wd);
    if (fgets(input_buf, 512, stdin) == NULL) {
        fprintf(stderr, "failed to read input!\n");
        return false;
    }
    return execute_command(input_buf);
}

int main(void) {
    bool exit = false;
    greet();
    while (!exit) {
        exit = prompt();
    }
    return 0;
}
