#include "parser.h"
#include "list.h"
#include "lexer.h"
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <stdio.h>

bool get_chk(token_t *stream, size_t *index, size_t len, token_t **out) {
    if (*index >= len) {
        return false;
    } else {
        *out = stream + *index;
        *index = *index + 1;
        return true;
    }
}

void kind_str(token_kind kind, char *output) {
    switch (kind) {
        case None:
            strcpy(output, "None");
            break;
        case String:
            strcpy(output, "String");
            break;
        case Number:
            strcpy(output, "Number");
            break;
        case Word:
            strcpy(output, "Word");
            break;
        case Pipe:
            strcpy(output, "Pipe");
            break;
        case AngleLeft:
            strcpy(output, "AngleLeft");
            break;
        case AngleRight:
            strcpy(output, "AngleRight");
            break;
        case DAngleLeft:
            strcpy(output, "DAngleLeft");
            break;
        case DAngleRight:
            strcpy(output, "DAngleRight");
            break;
        case Equal:
            strcpy(output, "Equal");
            break;
        case NewLine:
            strcpy(output, "NewLine");
            break;
        default:
            strcpy(output, "Unimplemented!");
            break;
    }
}

void expect(token_kind kind, token_t *found) {
    char expected_buf[128];
    char found_buf[128];
    kind_str(kind, expected_buf);
    kind_str(found->kind, found_buf);
    fprintf(stderr, "expected %s found %s instead!\n", expected_buf, found_buf);
}

ast_node_t *make_node(node_kind kind, ast_node_t *left, ast_node_t *right, void *value) {
    ast_node_t *node = (ast_node_t*) malloc(sizeof(ast_node_t));
    node->kind = kind;
    node->left = left;
    node->right = right;
    node->value = value;
    return node;
}

ast_node_t *parse_value_list(token_t *stream, size_t *index, size_t len) {
    if (len == 0) return NULL;
    list_node_t *head = make_head();
    list_node_t *tail = head;
    token_t *next = NULL;
    while ((get_chk(stream, index, len, &next))) {
        if (next->kind == NewLine) continue;
        if (next->kind == Word || next->kind == Number || next->kind == String) {
            append_node(&tail, next);
        } else {
            *index = *index - 1;
            break;
        }
    }
    if (head->next == NULL) {
        return NULL;
    }
    return make_node(ArgumentList, NULL, NULL, head);
}

ast_node_t *parse_simple_command(token_t *stream, size_t *sindex, size_t slen, bool *fatal) {
    size_t restore = *sindex;
    token_t *word = NULL;
    if (!get_chk(stream, sindex, slen, &word)) return NULL;
    if (word != NULL && word->kind == Word) {
        ast_node_t *cmd = make_node(Value, NULL, NULL, word);
        ast_node_t *args = parse_value_list(stream, sindex, slen);
        return make_node(SimpleCommand, cmd, args, NULL);
    }
    *sindex = restore;
    return NULL;
}

ast_node_t *parse_redirection(token_t *stream, size_t *sindex, size_t slen, bool *fatal) {
    size_t restore = *sindex;
    ast_node_t *fd = NULL;
    token_t *op = NULL;
    if (!get_chk(stream, sindex, slen, &op)) return NULL;
    // TODO: update this to support fds in the lexer level
    if (op->kind == Number) {
        fd = make_node(Value, NULL, NULL, op);
        if (!get_chk(stream, sindex, slen, &op)) return NULL;
    }
    if (op->kind == AngleLeft || op->kind == DAngleLeft || op->kind == AngleRight || op->kind == DAngleRight) {
        token_t *bound = NULL;
        if (!get_chk(stream, sindex, slen, &bound)) return NULL;
        if (bound->kind == Word) {
            ast_node_t *bound_file = make_node(Value, NULL, NULL, bound);
            return make_node(RedirectionNode, fd, bound_file, op);
        } else {
            expect(Word, bound);
            *fatal = true;
        }
    }
    *sindex = restore;
    return NULL;
}

ast_node_t *parse_command(token_t *stream, size_t len) {
    size_t i = 0;
    bool fatal = false;
    ast_node_t *command = parse_simple_command(stream, &i, len, &fatal);
    ast_node_t *redirection_chain = NULL;
    list_node_t *head = make_head();
    list_node_t *tail = head;
    if (fatal) return NULL;
    while (true) {
        ast_node_t *redirection = parse_redirection(stream, &i, len, &fatal);
        if (fatal) return NULL;
        if (redirection == NULL) {
            if (head->next == NULL) return command;
            else {
                ast_node_t *redirection_list = make_node(RedirectionChain, NULL, NULL, head);
                return make_node(Command, command, redirection_list, NULL);
            }
        }
        append_node(&tail, redirection);
    }
}


ast_node_t *parse(char *text) {
    token_t tokens[1024];
    lexer_error_t lexer_error;
    int32_t len = scan(text, tokens, &lexer_error);
    if (len == 0) return NULL;
    return parse_command(tokens, len);
}
