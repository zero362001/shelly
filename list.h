#ifndef list_h
#define list_h
#include <stdlib.h>

typedef struct list_node {
    struct list_node *next;
    void *data;
} list_node_t;

void append_node(list_node_t **tail, void *data);
void remove_node(list_node_t **head, void *data);
void remove_node_at(list_node_t **head, size_t pos);
list_node_t *make_head();
void clear(list_node_t *head);

#endif
