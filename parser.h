#ifndef parser_h
#define parser_h

typedef enum {
    SimpleCommand,
    Assignment,
    ArgumentList,
    Value,
    RedirectionNode,
    RedirectionChain,
    Command
} node_kind;

typedef struct ast_node {
    node_kind kind;
    struct ast_node *left;
    struct ast_node *right;
    void *value;
} ast_node_t;

ast_node_t *parse(char *text);

#endif
