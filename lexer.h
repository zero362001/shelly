#ifndef lexer_h
#define lexer_h

#include <stdint.h>
#include <stdlib.h>

typedef enum {
    None,
    String,
    Number,
    Word,
    Pipe,
    AngleLeft,
    AngleRight,
    DAngleLeft,
    DAngleRight,
    Equal,
    NewLine
} token_kind;

typedef struct {
    token_kind kind;
    size_t line, column;
    char lexeme[128];
    size_t len;
} token_t;

typedef struct {
    size_t line, column;
    char message[1024];
} lexer_error_t;

int32_t scan(char *code, token_t *stream, lexer_error_t *error);
void test();

#endif
