#include "list.h"

void append_node(list_node_t **tail, void *data) {
    if (data == NULL) return;
    list_node_t *new_node = (list_node_t*) malloc(sizeof(list_node_t));
    new_node->data = data;
    new_node->next = NULL;
    (*tail)->next = new_node;
    *tail = new_node;
}

void remove_node(list_node_t **head, void *data) {
    list_node_t *prev = NULL;
    list_node_t *iter = *head;
    while (iter) {
        if (iter->data == data) {
            if (prev)
                prev->next = iter->next;
            else *head = (*head)->next;
            free(iter);
        }
        prev = iter;
        iter = iter->next;
    }
}

void remove_node_at(list_node_t **head, size_t pos) {
    size_t i = 0;
    list_node_t *prev = NULL;
    list_node_t *iter = *head;
    while ((i++ < pos && iter != NULL)) {
        prev = iter;
        iter = iter->next;
    }
    if (iter == NULL) return;
    else {
        if (prev) prev->next = iter->next;
        else *head = (*head)->next;
        free(iter);
    }
}

list_node_t *make_head() {
    list_node_t *head = (list_node_t*)malloc(sizeof(head));
    head->next = NULL;
    head->data = NULL;
    return head;
}

void clear(list_node_t *head) {
    while (head != NULL) {
        list_node_t *next = head->next;
        free(head);
        head = next;
    }
}
