#ifndef redir_h
#define redir_h

typedef struct {
    int orig_fd;
    int new_fd;
} redirection_t;

#endif
