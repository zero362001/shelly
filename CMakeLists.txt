project(shelly)
cmake_minimum_required(VERSION 3.5)
set(CMAKE_C_STANDARD 11)

add_executable(shelly parser.c shelly.c list.c lexer.c)
